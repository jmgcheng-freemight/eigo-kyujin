$(document).ready(function(){

	/**/
	var o_form_apply = $('#frm_apply');
	var o_popup_inner = $('.popup-inner');
	var o_popup_close = $('.popup-close-link');

	/*
		popup
	*/
	set_popup_inner_size();
	window.addEventListener('resize', set_popup_inner_size);
	function set_popup_inner_size()
	{
		var i_popup_inner_size = window.innerHeight - 50;
		o_popup_inner.css('height', i_popup_inner_size + 'px');
	}
	o_popup_inner.on('click', function(e){
		if (e.target == this) { 
			$(this).parent('.popup').addClass('is-hidden');
        }
	});
	o_popup_close.on('click', function(e){
		e.preventDefault();
		$(this).parents('.popup').addClass('is-hidden');
	});
	// contact popup
	$('.apply-btn-popup').on('click', function(e){
		e.preventDefault();
		if( !o_form_apply.parents('.popup').is(':visible') )
		{
			o_form_apply.parents('.popup').removeClass('is-hidden');
		} 
		else 
		{  
			o_form_apply.parents('.popup').addClass('is-hidden');
		}
	});

});

angular.module('siteApp', [])
	.controller('applyCtrl', ['$scope', '$http', function($scope) {

		$scope.getDate = function() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			if(dd<10) {
			dd='0'+dd
			} 
			if(mm<10) {
			mm='0'+mm
			} 
			today = mm+'/'+dd+'/'+yyyy;
			return today;
		};


		$scope.applySubmit = function() {
			console.log('applySubmit');
			var b_spreadSheetSent = false;
			var b_emailSent = false;
			var a_fields = $('#frm_apply').serializeArray();
			//console.log(a_fields);
			var o_params_temp = {};
			o_params_temp['frm_apply_time_stamp'] = $scope.getDate();
			a_fields.forEach( function (o_item)
			{
				o_params_temp[ o_item.name ] = o_item.value;
			});
			//console.log(o_params_temp);
			var o_params = {
				'time_stamp':o_params_temp.frm_apply_time_stamp, 
				'name':o_params_temp.frm_apply_name, 
				'phonetic':o_params_temp.frm_apply_phonetic, 
				'contact_number':o_params_temp.frm_apply_contact_number, 
				'mail':o_params_temp.frm_apply_mail, 
				'location':o_params_temp.frm_apply_location, 
				'country':o_params_temp.frm_apply_country, 
				'birth_year':o_params_temp.frm_apply_birth_year, 
				'birth_month':o_params_temp.frm_apply_birth_month, 
				'birth_day':o_params_temp.frm_apply_birth_day, 
				'sex':o_params_temp.frm_apply_sex, 
				'highest_education':o_params_temp.frm_apply_highest_education, 
				'school':o_params_temp.frm_apply_school, 
				'current_status':o_params_temp.frm_apply_current_status, 
				'job_change_time':o_params_temp.frm_apply_job_change_time, 
				'income_min':o_params_temp.frm_apply_income_min, 
				'income_max':o_params_temp.frm_apply_income_max, 
				'prefered_location_1':o_params_temp.frm_apply_prefered_location_1, 
				'prefered_location_2':o_params_temp.frm_apply_prefered_location_2, 
				'prefered_location_3':o_params_temp.frm_apply_prefered_location_3, 
				'prefered_industry_1':o_params_temp.frm_apply_prefered_industry_1, 
				'prefered_industry_2':o_params_temp.frm_apply_prefered_industry_2, 
				'prefered_industry_3':o_params_temp.frm_apply_prefered_industry_3, 
				'prefered_job_category':o_params_temp.frm_apply_prefered_job_category, 
				'prefered_job_category_2':o_params_temp.frm_apply_prefered_job_category_2, 
				'prefered_job_category_3':o_params_temp.frm_apply_prefered_job_category_3, 
				'english_skill':o_params_temp.frm_apply_english_skill, 
				'toeic':o_params_temp.frm_apply_toeic,
				'other_lang_remarks':o_params_temp.frm_apply_other_lang_remarks, 
				'self_pr':o_params_temp.frm_apply_self_pr, 
				'resume':'', 
				'other_pref':o_params_temp.frm_apply_other_pref, 
				'interview_pref':o_params_temp.frm_apply_interview_pref, 
				'referral':o_params_temp.frm_apply_referral
			};
			console.log(o_params);

			//push to google spreadSheet
			$.ajax({
				url: "https://script.google.com/macros/s/AKfycbxWusRaqaRT8x95z7G1gts-cY5GJIRGm8zB-0Yrmvv489YpkVSm/exec?prefix=?",
				dataType: "jsonp",
				data: o_params,
				beforeSend: function() {
					$('.form-apply-ajax-msg').hide();
					$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').hide();
					$('.form-apply-ajax-msg .form-apply-ajax-msg-sent').hide();
					$('.form-apply-ajax-msg .form-apply-ajax-msg-error').hide();
					$('.form-apply-submit-btn').hide();
					
					$('.form-apply-ajax-msg').show();
					$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').show();
				},
				success: function( o_response_sheet ) {
					console.log( o_response_sheet );
					if( o_response_sheet.status == true )
					{
						b_spreadSheetSent = true;

						if( b_spreadSheetSent )
						{
							//try sending email notification
							$.ajax({
								url: "mail-notification.php",
								type: "POST",
								dataType: "json",
								data: o_params,
								success: function( o_response_email ) {
									console.log( o_response_email );				
									
									if( o_response_email.result == true )
									{
										b_emailSent = true;
										$('.form-apply-submit-btn').hide();
										$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').hide();
										$('.form-apply-ajax-msg .form-apply-ajax-msg-sent').show();
									}
									else
									{
										console.log('fail email');
										$('.form-apply-ajax-msg').hide();
										$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').hide();
										$('.form-apply-submit-btn').show();
									}
								}
							});
						}
						else
						{
							console.log('fail sheet');
							$('.form-apply-ajax-msg').hide();
							$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').hide();
							$('.form-apply-submit-btn').show();
						}
					}
					else
					{
						console.log('fail sent');
						$('.form-apply-ajax-msg').hide();
						$('.form-apply-ajax-msg .form-apply-ajax-msg-sending').hide();
						$('.form-apply-submit-btn').show();
					}
				}
			});


				

		}


	}]);
