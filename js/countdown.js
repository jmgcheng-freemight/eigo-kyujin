$(function() {
	countDown();
 });
 
 function countDown() {
 	var startTime = new Date();
 	//カウントダウンの終了期日を記入↓
    var endTime = new Date('July 31,2015, 23:59:59');
    var diff  = endTime - startTime;
    var times = 24 * 60 * 60 * 1000;
    var day   = Math.floor(diff / times)
    var hour  = Math.floor(diff % times / (60 * 60 * 1000))
    var min   = Math.floor(diff % times / (60 * 1000)) % 60
    var sec   = Math.floor(diff % times / 1000) % 60 % 60
    if(diff > 0){
    	$("#Timer").text('キャンペーン終了まで  残り' + day + '日' + hour + '時間' + min + '分' + sec +'秒');
    	setTimeout('countDown()', 10);
    } else {
    	//終了した時のテキスト
    	$("#Timer").text('７月末のキャンペーンは終了しました！');
    }
 }