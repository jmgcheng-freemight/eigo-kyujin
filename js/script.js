/****************************************
[script] script.js
****************************************/

// getComputedStyle polyfill https://github.com/jonathantneal/Polyfills-for-IE8/blob/master/getComputedStyle.js
!("getComputedStyle" in this)&&(this.getComputedStyle=(function(){function d(g,h,j,i){var l=h[j],f=parseFloat(l),k=l.split(/\d/)[0],e;i=i!=null?i:/%|em/.test(k)&&g.parentElement?d(g.parentElement,g.parentElement.currentStyle,"fontSize",null):16;e=j=="fontSize"?i:/width/i.test(j)?g.clientWidth:g.clientHeight;return(k=="em")?f*i:(k=="in")?f*96:(k=="pt")?f*96/72:(k=="%")?f/100*e:f}function b(h,j){var k=j=="border"?"Width":"",g=j+"Top"+k,i=j+"Right"+k,e=j+"Bottom"+k,f=j+"Left"+k;h[j]=(h[g]==h[i]==h[e]==h[f]?[h[g]]:h[g]==h[e]&&h[f]==h[i]?[h[g],h[i]]:h[f]==h[i]?[h[g],h[i],h[e]]:[h[g],h[i],h[e],h[f]]).join(" ")}function a(f){var e=f.currentStyle,g=this,h=d(f,e,"fontSize",null);for(property in e){if(/width|height|margin.|padding.|border.+W/.test(property)&&g[property]!=="auto"){g[property]=d(f,e,property,h)+"px"}else{if(property==="styleFloat"){g["float"]=e[property]}else{g[property]=e[property]}}}b(g,"margin");b(g,"padding");b(g,"border");g.fontSize=h+"px";return g}a.prototype={constructor:a,getPropertyPriority:function(){},getPropertyValue:function(e){return this[e]||""},item:function(){},removeProperty:function(){},setProperty:function(){},getPropertyCSSValue:function(){}};function c(e){return new a(e)}return c})(this));

/****************************************/

// addEventListener polyfill https://github.com/jonathantneal/Polyfills-for-IE8/blob/master/eventListener.js
("Element" in this)&&!("addEventListener" in this.Element.prototype)&&(function(b){function a(h,g){var f=this;for(property in h){f[property]=h[property]}f.currentTarget=g;f.target=h.srcElement||g;f.timeStamp=+new Date;f.preventDefault=function(){h.returnValue=false};f.stopPropagation=function(){h.cancelBubble=true}}function d(h,i){var f=this,g=f.listeners=f.listeners||[],e=g.push([i,function(j){i.call(f,new a(j,f))}])-1;f.attachEvent("on"+h,g[e][1])}function c(h,j){for(var f=this,g=f.listeners||[],i=g.length,e=0;e<i;++e){if(g[e][0]===j){f.detachEvent("on"+h,g[e][1])}}}b.addEventListener=document.addEventListener=b.Element.prototype.addEventListener=d;b.removeEventListener=document.removeEventListener=b.Element.prototype.removeEventListener=c})(this);

/****************************************/

// set requestAnimationFrame
var requestAnimationFrame = (function(){
return window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame || 
function(callback){window.setTimeout(callback, 1000.0 / 60.0);};
})();

/****************************************/

// 画面の上下の位置を0にセットする関数
var windowHeight = window.innerHeight;// 画面の高さを取得
var windowPosBottom= windowHeight +(windowHeight * -1);// 画面の下を0に
var windowPosTop= windowHeight - windowHeight;// 画面の上を0

/****************************************/

// html class
var htmlElement = document.querySelector('html');

/****************************************/

// ontouchstart http://alxgbsn.co.uk/2011/10/17/enable-css-active-pseudo-styles-in-mobile-safari/
document.body.addEventListener("touchstart", function() {},false);

/****************************************/

// root rem calc
var rem = function rem(){return function(){return parseInt(window.getComputedStyle(htmlElement)['fontSize']);}}();

/****************************************
[script] supports.ms-touch.js
* base-code - https://github.com/izilla/Supports-Touch
* Copyright (c) 2013 Izilla Partners Pty Ltd
* Released under the MIT license.
****************************************/

;var supports = (function(){var html = document.documentElement,touch = navigator.msMaxTouchPoints;if (touch){html.className += ' ms-touch';return{touch:true}}else{html.className += ' no-ms-touch';return{touch:false}}})();

/****************************************/

// fixed touch device position:fixed focus form via http://dansajin.com/2012/12/07/fix-position-fixed/
var formTextInput=document.querySelectorAll('input[type="text"], input[type="email"], input[type="tel"], input[type="search"], input[type="url"], input[type="password"], input[type="datetime"], input[type="file"], input[type="color"], input[type="time"], input[type="range"], input[type="number"], input[type="date"], input[type="datetime-local"], input[type="month"], input[type="week"], textarea');
var fixfixedClassName="fixfixed";
var fixAddClassSelector="html";
for(i=0;i<formTextInput.length;i++){formTextInput[i].addEventListener("focus",function(){document.querySelector(fixAddClassSelector).classList.add(fixfixedClassName)});formTextInput[i].addEventListener("blur",function(){document.querySelector(fixAddClassSelector).classList.remove(fixfixedClassName)})};

/****************************************/

// click show / hide nav
var toggleNavOpenClass = 'nav-open';
var toggleNavLinkName = '.nav-toggle';
var toggleNavCloseLinkName = '.nav-close';

var toggleNavLink  = document.querySelector(toggleNavLinkName);
var toggleNavCloseLink  = document.querySelector(toggleNavCloseLinkName);
function pagenav(encodeURI){
var body = document.getElementsByTagName('body')[0];
if (body.classList.contains(toggleNavOpenClass) == true){
body.classList.remove(toggleNavOpenClass);} else {body.classList.add(toggleNavOpenClass);}
}

var pagenavMethod = function(e){pagenav();e.preventDefault();};
toggleNavLink.addEventListener('click',pagenavMethod,false);
toggleNavCloseLink.addEventListener('click',pagenavMethod,false);

/****************************************/

// sticky
function sticky(){
var stickyClassName = 'sticky';
var stickyAddClassSelector = 'html';
var remsize = 10; // top-height
var htmlRemSize = rem() * remsize;
var scrollPosition = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
if(scrollPosition > htmlRemSize){
document.querySelector(stickyAddClassSelector).classList.add(stickyClassName);
}else{
document.querySelector(stickyAddClassSelector).classList.remove(stickyClassName);
}}

var stickyMethod = function(){sticky();};
window.addEventListener('scroll',stickyMethod,false);
window.addEventListener('load',stickyMethod,false);

/****************************************
[script] smooth-scroll.js
* https://github.com/cferdinandi/smooth-scroll
* Animate scrolling to anchor links, by Chris Ferdinandi.
* Released under the MIT license.
****************************************/

window.smoothScroll=(function(f,h,b){var d={speed:500,easing:"easeInOutCubic",offset:0,updateURL:false,callbackBefore:function(){},callbackAfter:function(){}};var j=function(n,o){for(var m in o){n[m]=o[m]}return n};var e=function(m,n){if(m=="easeInQuad"){return n*n}if(m=="easeOutQuad"){return n*(2-n)}if(m=="easeInOutQuad"){return n<0.5?2*n*n:-1+(4-2*n)*n}if(m=="easeInCubic"){return n*n*n}if(m=="easeOutCubic"){return(--n)*n*n+1}if(m=="easeInOutCubic"){return n<0.5?4*n*n*n:(n-1)*(2*n-2)*(2*n-2)+1}if(m=="easeInQuart"){return n*n*n*n}if(m=="easeOutQuart"){return 1-(--n)*n*n*n}if(m=="easeInOutQuart"){return n<0.5?8*n*n*n*n:1-8*(--n)*n*n*n}if(m=="easeInQuint"){return n*n*n*n*n}if(m=="easeOutQuint"){return 1+(--n)*n*n*n*n}if(m=="easeInOutQuint"){return n<0.5?16*n*n*n*n*n:1+16*(--n)*n*n*n*n}return n};var g=function(o,n,p){var m=0;if(o.offsetParent){do{m+=o.offsetTop;o=o.offsetParent}while(o)}m=m-n-p;if(m>=0){return m}else{return 0}};var k=function(){return Math.max(h.body.scrollHeight,h.documentElement.scrollHeight,h.body.offsetHeight,h.documentElement.offsetHeight,h.body.clientHeight,h.documentElement.clientHeight)};var a=function(m){if(m===null||m===b){return{}}else{var n={};m=m.split(";");m.forEach(function(o){o=o.trim();if(o!==""){o=o.split(":");n[o[0]]=o[1].trim()}});return n}};var i=function(n,m){if((m===true||m==="true")&&history.pushState){history.pushState({pos:n.id},"",n)}};var c=function(x,r,n,B){n=j(d,n||{});var v=a(x?x.getAttribute("data-options"):null);var C=parseInt(v.speed||n.speed,10);var w=v.easing||n.easing;var q=parseInt(v.offset||n.offset,10);var G=v.updateURL||n.updateURL;var H=h.querySelector("[data-scroll-header]");var A=H===null?0:(H.offsetHeight+H.offsetTop);var u=f.pageYOffset;var m=g(h.querySelector(r),A,q);var z;var o=m-u;var s=k();var E=0;var t,F;if(x&&x.tagName==="A"&&B){B.preventDefault()}i(r,G);var y=function(I,L,J){var K=f.pageYOffset;if(I==L||K==L||((f.innerHeight+K)>=s)){clearInterval(J);n.callbackAfter(x,r)}};var p=function(){E+=16;t=(E/C);t=(t>1)?1:t;F=u+(o*e(w,t));f.scrollTo(0,Math.floor(F));y(F,m,z)};var D=function(){n.callbackBefore(x,r);z=setInterval(p,16)};if(f.pageYOffset===0){f.scrollTo(0,0)}D()};var l=function(m){if("querySelector" in h&&"addEventListener" in f&&Array.prototype.forEach){m=j(d,m||{});var n=h.querySelectorAll("[data-scroll]");Array.prototype.forEach.call(n,function(o,p){o.addEventListener("click",c.bind(null,o,o.getAttribute("href"),m),false)})}};return{init:l,animateScroll:c}})(window,document);

// <a href="#" data-scroll>Quad</a><br>
smoothScroll.init({
offset:0,
easing:'easeInOutCubic',
updateURL:false,
});

/****************************************/